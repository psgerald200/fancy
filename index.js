$(document).ready(() => {
    let homeNav = $("#toHome");
    let aboutNav = $("#toAbout");
    let galleriesNav = $("#toGalleries");
    let auctionsNav = $("#toAuctions");
    let registrationNav = $("#toRegistration");

    let homePage = $("#homePage");
    let galleriesPage = $("#galleriesPage");
    let auctionsPage = $("#auctionsPage");
    let registrationPage = $("#registrationPage");
    let aboutPage = $("#aboutPage");

    function hideAllPages() {
        homePage.css('display', 'none');
        galleriesPage.css('display', 'none');
        auctionsPage.css('display', 'none');
        registrationPage.css('display', 'none');
        aboutPage.css('display', 'none');
    }

    // ============ HOME PAGE ==============
    homeNav.click(() => {
        hideAllPages();
        homePage.css('display', 'block');
    });

    let slideImage = $("#slide-image");
    let slideLeft = $("#slideLeft");
    let slideRight = $("#slideRight");
    
    let images = ['assets/example-img.png', 'assets/example-img2.png'];
    let currImage = 0;
    slideImage.attr('src', images[currImage]);

    slideLeft.click(() => {
        if (currImage > 0) {
            currImage--;
            slideImage.attr('src', images[currImage]);
        }
    })

    slideRight.click(() => {
        if (currImage < images.length - 1) {
            currImage++;
            slideImage.attr('src', images[currImage]);
        }
    })
    // ============ HOME PAGE ==============
    
    
    // ============ About PAGE ==============
    aboutNav.click(() => {
        hideAllPages();
        aboutPage.css('display', 'block');
    });

    function magnify(imgID, zoom) {
        var img, glass, w, h, bw;
        img = document.getElementById(imgID);
        glass = document.createElement("DIV");
        glass.setAttribute("class", "img-magnifier-glass");
        img.parentElement.insertBefore(glass, img);
        glass.style.backgroundImage = "url('" + img.src + "')";
        glass.style.backgroundRepeat = "no-repeat";
        glass.style.backgroundSize = (img.width * zoom) + "px " + (img.height * zoom) + "px";
        bw = 3;
        w = glass.offsetWidth / 2;
        h = glass.offsetHeight / 2;
        glass.addEventListener("mousemove", moveMagnifier);
        img.addEventListener("mousemove", moveMagnifier);
        glass.addEventListener("touchmove", moveMagnifier);
        img.addEventListener("touchmove", moveMagnifier);
        function moveMagnifier(e) {
          var pos, x, y;
          e.preventDefault();
          pos = getCursorPos(e);
          x = pos.x;
          y = pos.y;
          if (x > img.width - (w / zoom)) {x = img.width - (w / zoom);}
          if (x < w / zoom) {x = w / zoom;}
          if (y > img.height - (h / zoom)) {y = img.height - (h / zoom);}
          if (y < h / zoom) {y = h / zoom;}
          glass.style.left = (x - w) + "px";
          glass.style.top = (y - h) + "px";
          glass.style.backgroundPosition = "-" + ((x * zoom) - w + bw) + "px -" + ((y * zoom) - h + bw) + "px";
        }
        function getCursorPos(e) {
          var a, x = 0, y = 0;
          e = e || window.event;
          a = img.getBoundingClientRect();
          x = e.pageX - a.left;
          y = e.pageY - a.top;
          x = x - window.pageXOffset;
          y = y - window.pageYOffset;
          return {x : x, y : y};
        }
    }
    magnify("map", 3);

    // ============ About PAGE ==============


    // ============ Galleries PAGE ==============
    galleriesNav.click(() => {
        hideAllPages();
        galleriesPage.css('display', 'block');
    });
    // ============ Galleries PAGE ==============


    // ============ Auctions PAGE ==============
    auctionsNav.click(() => {
        hideAllPages();
        auctionsPage.css('display', 'block');
    });

    function setTimer(auctionItem, auctionDeadline, timeLeft) {
        let timer = setInterval(() => {
            timeLeft--;
            
            const HOUR = Math.floor(timeLeft / 3600);
            const MIN = Math.floor((timeLeft%3600) / 60);
            const SEC = timeLeft % 60;
            auctionDeadline.html(`${HOUR}h ${MIN}m ${SEC}s left`);
    
            if (timeLeft === -1) {
                auctionDeadline.html('TIME IS UP');
                clearInterval(timer);
                auctionItem.fadeOut(1000);
            }
        }, 1000);
    }

    setTimer($("#auctionItem1"), $("#auctionDeadline1"), 120);
    setTimer($("#auctionItem2"), $("#auctionDeadline2"), 300);
    setTimer($("#auctionItem3"), $("#auctionDeadline3"), 3660);
    setTimer($("#auctionItem4"), $("#auctionDeadline4"), 47);
    setTimer($("#auctionItem5"), $("#auctionDeadline5"), 10800);
    setTimer($("#auctionItem6"), $("#auctionDeadline6"), 10800);
    
    var dt = new Date();
    var options = {'weekday': 'long', 'month': 'long', 'day': '2-digit', 'year': 'numeric'};
    var date = dt.toLocaleString('us-US', options);
    var x = document.getElementsByClassName("bidDate");

    for(i = 0; i < 4; i++)
    {
        x[i].innerHTML = date;
    }
    
    var y = document.getElementsByClassName("oncomingBidDate");
    for(i = 0; i < 2; i++)
    {
        y[i].innerHTML = date;
    }
    
    console.log(date);
    // ============ Auctions PAGE ==============


    // ============ Registration PAGE ==============
    registrationNav.click(() => {
        hideAllPages();
        registrationPage.css('display', 'block');
    });

    let regForm = $("#regForm");
    let userNameInput = $("#username");
    let fullNameInput = $("#fullname");
    let emailInput = $("#email");
    let passwordInput = $("#password");
    let countryInput = $("#country");
    let addressInput = $("#address");
    let artPrefInput = $("#artPreference");
    let submitBtn = $("#submitBtn");

    let userInfo = {
        userName: '',
        fullName: '',
        email: '',
        password: '',
        country: '',
        address: '',
        artPref: ''
    }

    userNameInput.change(e => {
        userInfo.userName = e.target.value;
    });

    fullNameInput.change(e => {
        userInfo.fullName = e.target.value;
    });

    emailInput.change(e => {
        userInfo.email = e.target.value;
    });

    passwordInput.change(e => {
        userInfo.password = e.target.value;
    });
    
    countryInput.change(e => {
        userInfo.country = e.target.value;
    });
    
    addressInput.change(e => {
        userInfo.address = e.target.value;
    });
    
    artPrefInput.change(e => {
        userInfo.artPref = e.target.value;
    });

    submitBtn.click(e => {
        e.preventDefault();
        let flag = true;

        if (userInfo.userName === null || userInfo.userName === '' || userInfo.userName.length < 5 || userInfo.userName > 15) {
            $("#usernameWarning").css('display', 'block');            
            flag = false;
        } else {
            $("#usernameWarning").css('display', 'none');            
        }
        
        if (userInfo.fullName === null || userInfo.fullName === '' || userInfo.fullName.length < 2) {
            $("#fullnameWarning").css('display', 'block');
            flag = false;
        } else {
            $("#fullnameWarning").css('display', 'none');
        }

        if (userInfo.email === null || userInfo.email === '' || userInfo.email.indexOf('@') < 1 || userInfo.email.indexOf('.') < 3) {
            $("#emailWarning").css('display', 'block');
            flag = false;
        } else {
            $("#emailWarning").css('display', 'none');
        }

        if (userInfo.password === null || userInfo.password === '' || userInfo.password.length < 8) {
            $("#passwordWarning").css('display', 'block');
            flag = false;
        } else {
            $("#passwordWarning").css('display', 'none');
        }

        if (userInfo.artPref === null || userInfo.artPref === '') {
            $("#artPrefWarning").css('display', 'block');
            flag = false;
        } else {
            $("#artPrefWarning").css('display', 'none');
        }

        if (userInfo.country === null || userInfo.country === '') {
            $("#countryWarning").css('display', 'block');
            flag = false;
        } else {
            $("#countryWarning").css('display', 'none');
        }

        if (userInfo.address === null || userInfo.address === '') {
            $("#addressWarning").css('display', 'block');
            flag = false;
        } else {
            $("#addressWarning").css('display', 'none');
        }

        if (flag) {
            regForm.css('display', 'none');
    
            let successText = document.createElement('h2');
            successText.innerHTML = `You are successfully registered, ${userInfo.fullName}!`;
            registrationPage.append(successText);
    
            console.log(userInfo);
        }

    });
    // ============ Registration PAGE ==============

});

$("#slideshow > div:gt(0)").hide();

setInterval(function() {
  $('#slideshow > div:first')
    .fadeOut(1000)
    .next()
    .fadeIn(1000)
    .end()
    .appendTo('#slideshow');
}, 3000);